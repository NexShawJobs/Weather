//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by nsn on 1/18/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherTests: XCTestCase {
    var viewController: ViewController!
    var weatherR: WeatherResponse? //=WeatherResponse()

    override func setUp() {
        super.setUp()
        viewController = ViewController()
         weatherR = WeatherResponse()

        let bundle = Bundle(for: WeatherTests.self)
        let path = bundle.path(forResource: "Test_WeatherData", ofType: "json")
        do {
            let data:NSData =  try NSData(contentsOfFile: path!)
            do {
                self.weatherR = try JSONDecoder().decode(WeatherResponse.self, from: data as Data)
                //viewController.weatherResponse = weatherR//! as WeatherResponse
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }catch let jsonErr {
            print("Error in converting json file to Data", jsonErr)
        }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testTimeStampConvertedToCorrectReadableFormat(){
        XCTAssertEqual(viewController.formatTimeStamp(stamp: 1516072104), "Jan 15 2018 09:08 PM")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
