//
//  Services.swift
//  Weather
//
//  Created by nsn on 1/18/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit

class Services: NSObject {

    private static let API_KEY = "4e63f48bb2d090d7fb7d80f6447ace6a"
    static let baseURL = "http://api.openweathermap.org/data/2.5/weather"
    static let baseIconURL = "http://openweathermap.org/img/w/"
//    //CedarHill,TX75104,UnitedStates

    func fetchWeatherData(for strUrl:String, completion:@escaping (Data) -> ())  {
        guard let url = URL(string: strUrl) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else {
                return
            }
                completion(data)
                return
            }.resume()

    }

    func stringUrlForImage(forIconID iconID:String) -> String{
        return Services.baseIconURL + iconID + ".png"
    }
    func stringUrlForWeatherData(forLocation location:String) -> String{
        return  Services.baseURL  + "?APPID=\(Services.API_KEY)&\(location)"
    }
    
}
