//
//  ViewController.swift
//  Weather
//
//  Created by nsn on 1/18/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit
import GooglePlaces

class ViewController: UIViewController {

    @IBOutlet weak var changeCity: UIButton!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var temp_min: UILabel!
    @IBOutlet weak var temp_max: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet var viewModel: ViewModel!

    static let LAST_SEARCH_KEY = "lastSearchKey"
    static let DEFAULT_CITY = "q=CedarHill,TX,US"
    var weatherResponse: WeatherResponse = WeatherResponse()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.changeCity.titleLabel!.lineBreakMode = .byWordWrapping
        self.changeCity.titleLabel!.textAlignment = .center
        self.changeCity.setTitle("change \n city", for: .normal)

        for view in self.view.subviews{
            view.backgroundColor = UIColor.clear
        }

        //self.weatherResponse = WeatherResponse()
        self.loadDefaultWeatherData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // Present the Autocomplete view controller when the button is pressed.
    @IBAction func autocompleteClicked(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
}

extension ViewController: GMSAutocompleteViewControllerDelegate {

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
    }
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didSelect prediction: GMSAutocompletePrediction) -> Bool {
        let locationString = prediction.attributedFullText.string.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)

        self.getWeatherData(forLocation: "q=\(locationString)")
        return true
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }

    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    func getWeatherData(forLocation location:String){

        viewModel.fetchWeatherData(for:viewModel.stringUrlForWeatherData(forLocation: location), completion: {
        do {
            self.weatherResponse = try JSONDecoder().decode(WeatherResponse.self, from: self.viewModel.weatherData)

            self.getIconImage(for: self.weatherResponse.weather![0].icon!)
            DispatchQueue.main.async {
                let degree = NSString(format:"%@", "\u{00B0}") as String
                self.cityName.text = self.weatherResponse.name! + "\n" + self.formatTimeStamp(stamp: self.weatherResponse.dt!)
                self.weatherDescription.text = self.weatherResponse.weather![0].description
                self.temp.text = self.convertToFahrenheit(forKelvinTemp: self.weatherResponse.main!.temp!) + degree
                self.temp_min.text = self.convertToFahrenheit(forKelvinTemp: self.weatherResponse.main!.temp_min!) + "\(degree)\n Min"
                self.temp_max.text = self.convertToFahrenheit(forKelvinTemp: self.weatherResponse.main!.temp_max!) + "\(degree)\n Max"

                UserDefaults.standard.set(location, forKey: ViewController.LAST_SEARCH_KEY)
            }
        } catch let err {
            print(err)
            let alert = UIAlertController(title: "No Weather Data", message: "Weather data for the selected location is not found.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

        } )
    }

    func getIconImage(for iconID: String){
        viewModel.fetchWeatherData(for: viewModel.stringUrlForImage(forIconID: iconID), completion: {
            let img:UIImage =  UIImage(data: self.viewModel.weatherData)!
            DispatchQueue.main.async {
                self.weatherIconImageView.image = img
            }
        })
    }

    //MARK: --
    //MARK: HelperMethods
    func featchWeatherDataByGeoLocation(lat:Double, lon:Double){
        //api.openweathermap.org/data/2.5/weather?lat=35&lon=139
        let locationString = "lat=\(lat)&lon=\(lon)"
        self.getWeatherData(forLocation: locationString)
    }

    func loadDefaultWeatherData(){
        if let location:String = UserDefaults.standard.value(forKey: ViewController.LAST_SEARCH_KEY) as? String {
            self.getWeatherData(forLocation: location)

        }else {
            self.getWeatherData(forLocation: ViewController.DEFAULT_CITY)
        }
    }

    func convertToFahrenheit(forKelvinTemp temp: Double) -> String{
        let tempFara = (9/5*(temp - 273.15)) + 32
        let tempFaraStr = String(format: "%.f", arguments: [tempFara])
        return tempFaraStr
    }

    func formatTimeStamp(stamp:Double) -> String{
        let date = NSDate(timeIntervalSince1970: stamp)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY"
        return dayTimePeriodFormatter.string(from: date as Date)
    }
}
