//
//  ViewModel.swift
//  Weather
//
//  Created by nsn on 1/18/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit

struct WeatherResponse : Decodable {
    let coord:Coord?
    let weather:[Weather]?
    let base:String?
    let main:Main?
    let visibility:Int?
    let wind:Wind?
    let clouds:Clouds?
    let dt:Double?
    let sys:Sys?
    let id:Double?
    let name:String?
    let cod:Int?

    init(coord:Coord? = nil, weather:[Weather]? = nil, base:String? = nil,
         main:Main? = nil, visibility:Int? = nil, wind:Wind? = nil,
         clouds:Clouds? = nil, dt:Double? = nil, sys:Sys? = nil,
         id:Double? = nil, name:String? = nil, cod:Int? = nil){
        self.coord = coord
        self.weather = weather
        self.base = base
        self.main = main
        self.visibility = visibility
        self.wind = wind
        self.clouds = clouds
        self.dt = dt
        self.sys = sys
        self.id = id
        self.name = name
        self.cod = cod
    }
}

struct Coord : Decodable {
    let lon:Double?
    let lat:Double?
}
struct Weather : Decodable {
    let id:Int?
    let main:String?
    let description:String?
    let icon:String?
}

struct Main : Decodable {
    let temp:Double?
    let pressure:Float?
    let humidity:Int?
    let temp_min:Double?
    let temp_max:Double?
    let sea_level:Float?
    let grnd_level:Float?
}
struct Wind : Decodable {
    let speed:Float?
    let deg:Float?
}
struct Clouds : Decodable {
    let all:Int?
}
struct Sys : Decodable {
    let message:Double?
    let country:String?
    let sunrise:Double?
    let sunset:Double?
}

struct Icon {
    let icon:UIImage
    let urlString:String
}

class ViewModel: NSObject {

    @IBOutlet weak var services: Services!
    var weatherData:Data = Data()

    //get Data
    func fetchWeatherData(for urlString:String, completion:@escaping () -> ()){
        services.fetchWeatherData(for: urlString, completion: {data in
            self.weatherData = data
            completion()
        })
    }

    //URL for weather iCon
    func stringUrlForImage(forIconID iconID:String) -> String{
        return services.stringUrlForImage(forIconID:iconID)
    }

    //URL for weather data
    func stringUrlForWeatherData(forLocation location:String) -> String{
        return  services.stringUrlForWeatherData(forLocation: location)
    }
    
}
